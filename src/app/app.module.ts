import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import{AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './users/posts/posts.component';
import {PostsService} from './users/posts/posts.service';
import { PostComponent } from './users/post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';

const appRoutes: Routes = [
    { path: 'users', component: UsersComponent },
    { path: 'posts', component: PostsComponent },
    { path: '', component: UsersComponent },
    { path: '**', component: PageNotFoundComponent }
  ];

  export const firebaseConfig = {
    apiKey: "AIzaSyA9POLQeFJenN__T9MWeHR8T7SDiXi_NZs",
    authDomain: "angular-a3117.firebaseapp.com",
    databaseURL: "https://angular-a3117.firebaseio.com",
    storageBucket: "angular-a3117.appspot.com",
    messagingSenderId: "196007565624"
  }

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
