import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {

  users = [
    {name: 'Jeni', email: 'jeni@gmail.com'},
    {name: 'Harry', email: 'harry@gmail.com'},
    {name: 'George', email: 'george@gmail.com'},
  ]

  constructor() { }

  addUser(user){
    this.users.push(user);
  }

  ngOnInit() {
  }

}
