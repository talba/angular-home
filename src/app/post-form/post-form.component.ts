import {Post} from '../users/post/post';
import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'jce-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  @Output() postAddedEvent = new EventEmitter<Post>();
  post:Post = {title:'', author:'', body:''};

  constructor() { }

  onSubmit(form:NgForm){
    console.log(form);
    this.postAddedEvent.emit(this.post);
    this.post = {title:'', author:'', body:''};
  }

  ngOnInit() {
  }

}
