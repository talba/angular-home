import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs: ['post']
})
export class PostComponent implements OnInit {

  post:Post;
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() updateEvent = new EventEmitter<Post>();
  isEdit:Boolean = false;
  editButtonText:String = "Edit";
  tempPost:Post;

  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.post);
  }

  toggleEdit(){
    this.isEdit=!this.isEdit;
    this.isEdit ? this.editButtonText="Save" : this.editButtonText="Edit";
    this.tempPost=Object.assign({},this.post)
    if (!this.isEdit){
      this.updateEvent.emit(this.post);
    }
  }

  cancelEdit(){
    this.post=this.tempPost;
    this.isEdit=!this.isEdit;
    this.isEdit ? this.editButtonText="Save" : this.editButtonText="Edit";
  }

  ngOnInit() {
  }

}
