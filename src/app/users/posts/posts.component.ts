import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts;
  isLoading:Boolean= true;

  constructor(private _postService:PostsService) { }

  addPost(post){
    this._postService.addPost(post);
  }

  deletePost(post){
    this._postService.deletePost(post);
  }

  updatePost(post){
    this._postService.updatePost(post);
  }

  ngOnInit() {
    this._postService.getPosts().subscribe(postData => {
      this.posts=postData;
      this.isLoading=false;
    });
  }

}
